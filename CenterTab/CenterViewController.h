//
//  CenterViewController.h
//  CenterTab
//
//  Created by JOSEPH KERR on 5/22/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CenterViewControllerDelegate;


@interface CenterViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *transitionView;
@property (weak, nonatomic) id <CenterViewControllerDelegate> delegate;

@property (strong, nonatomic) UIView *magicView;

-(void)magic;

//-(void)addTrans:(UIView*)magicView;

@end


@protocol CenterViewControllerDelegate <NSObject>

-(void)didDismiss;

@end

//
//  MyTabBarControllerDelegate.m
//  CenterTab
//
//  Created by JOSEPH KERR on 5/22/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import "MyTabBarControllerDelegate.h"
#import "SecondViewController.h"
#import "CenterViewController.h"


@interface AAPLOverlayAnimatedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) BOOL isPresentation;
@property (nonatomic) BOOL isCenter;
@end

@implementation AAPLOverlayAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 0.5;
}

//#define USESTANDARDANIM

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    // Here, we perform the animations necessary for the transition
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *fromView = [fromVC view];
    UIViewController *toVC   = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *toView = [toVC view];
    
    UIView *containerView = [transitionContext containerView];
    
    BOOL isPresentation = [self isPresentation];
    
#ifdef USESTANDARDANIM
    
    if(isPresentation)
    {
        [containerView addSubview:toView];
    }
    
    UIViewController *animatingVC = isPresentation? toVC : fromVC;
    UIView *animatingView = [animatingVC view];
    
    CGRect appearedFrame = [transitionContext finalFrameForViewController:animatingVC];
    
    // Our dismissed frame is the same as our appeared frame, but off the right edge of the container
    CGRect dismissedFrame = appearedFrame;
    dismissedFrame.origin.x += dismissedFrame.size.width;

    CGRect initialFrame = isPresentation ? dismissedFrame : appearedFrame;
    CGRect finalFrame = isPresentation ? appearedFrame : dismissedFrame;
    
    [animatingView setFrame:initialFrame];
    
    // Animate using the duration from -transitionDuration:
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0
         usingSpringWithDamping:300.0
          initialSpringVelocity:5.0
                        options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [animatingView setFrame:finalFrame];
                     }
                     completion:^(BOOL finished){
                         // If we're dismissing, remove the presented view from the hierarchy
                         if(![self isPresentation])
                         {
                             [fromView removeFromSuperview];
                         }
                         // We need to notify the view controller system that the transition has finished
                         [transitionContext completeTransition:YES];
                     }];
    
#else
    
    if(isPresentation)
    {
        if ([self isCenter] == NO) {
            toView.hidden = NO;
        } else {
            toView.hidden = YES;
        }
        
        [containerView addSubview:toView];
    }
    
    UIViewController *animatingVC = isPresentation? toVC : fromVC;
    UIView *animatingView = [animatingVC view];
    
    CGRect appearedFrame = [transitionContext finalFrameForViewController:animatingVC];

    CGRect finalFrame = appearedFrame;
    
    [animatingView setFrame:finalFrame];
    
    if([self isPresentation] == NO) {
        [fromView removeFromSuperview];
    }

    // We need to notify the view controller system that the transition has finished
    [transitionContext completeTransition:YES];
    
#endif
    
    
}

@end




@interface MyTabBarControllerDelegate () <CenterViewControllerDelegate>
@property (nonatomic,strong) UIView *snapShotView;
@property BOOL isCenter;
@property  NSUInteger previousTab;
@property (nonatomic,weak) UITabBarController *saveTabBarController;
@end


@implementation MyTabBarControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)tabBarController:(UITabBarController *)tabBarController
           animationControllerForTransitionFromViewController:(UIViewController *)fromVC
                                             toViewController:(UIViewController *)toVC
{
    NSLog(@"%s",__func__);

    AAPLOverlayAnimatedTransitioning *at =  [AAPLOverlayAnimatedTransitioning new];
    at.isPresentation = YES;
    at.isCenter = self.isCenter;
    return at;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSLog(@"%s",__func__);

    if ([viewController isKindOfClass:[SecondViewController class]]) {

        self.previousTab = tabBarController.selectedIndex;
        self.isCenter = YES;
        self.snapShotView = [tabBarController.view snapshotViewAfterScreenUpdates:YES];
        self.snapShotView.frame = tabBarController.view.bounds;
        
    } else {
        self.isCenter = NO;
    }
    
    return YES;
}


-(void)didDismiss {
    [self.saveTabBarController setSelectedIndex:self.previousTab];
}

- (void)tabBarController:(UITabBarController *)tabBarController
 didSelectViewController:(UIViewController *)viewController
{
    NSLog(@"%s",__func__);

    //UIImage *screenImage = [self snapshot:tabBarController.view];
    
    if ([viewController isKindOfClass:[SecondViewController class]]) {
        
        self.saveTabBarController = tabBarController;
        
        CenterViewController *cvc = [tabBarController.storyboard instantiateViewControllerWithIdentifier:@"CenterViewController"];
        //        cvc.transitionView = self.snapShotView;
        
        cvc.delegate = self;
        
        cvc.magicView = self.snapShotView;
        
        [tabBarController presentViewController:cvc animated:NO completion:^{
            [cvc magic];
        }];
        
    }
    
}


@end


//        CenterViewController *cvc = [tabBarController.storyboard instantiateViewControllerWithIdentifier:@"CenterViewController"];
////        cvc.transitionView = self.snapShotView;
//
//        [cvc addTrans:self.snapShotView];
//
//        [tabBarController presentViewController:cvc animated:NO completion:^{
//            [cvc magic];
//        }];

//        //CGRect newFr = tabBarController.bounds;
//        CGRect newFr = CGRectOffset(tabBarController.view.bounds, 0, tabBarController.view.bounds.size.height);
//        [UIView animateWithDuration:2.0 delay:1.0 options:0 animations:^{
//            cvc.transitionView.frame = newFr;
//        } completion:nil
//        ];

//------

//        UIView *snapView = [tabBarController.view snapshotViewAfterScreenUpdates:YES];
//        CenterViewController *cvc = [tabBarController.storyboard instantiateViewControllerWithIdentifier:@"CenterViewController"];
//        cvc.transitionView = snapView;
//
//        [tabBarController presentViewController:cvc animated:NO completion:nil];
//
//        //CGRect newFr = tabBarController.bounds;
//        CGRect newFr = CGRectOffset(tabBarController.view.bounds, 0, tabBarController.view.bounds.size.height);
//
//        [UIView animateWithDuration:2.0 animations:^{
//            cvc.transitionView.frame = newFr;
//        }];

//------

//        [tabBarController performSegueWithIdentifier:@"CenterPresent2" sender:self];

//------
//        [tabBarController performSegueWithIdentifier:@"CenterPresent" sender:nil];


//------

//        UIViewController *contentViewController = [UIViewController new];
//
//        contentViewController.view.backgroundColor = [UIColor blueColor];
//
//        [tabBarController presentViewController:contentViewController animated:YES completion:nil];


//- (UIImage *)snapshot:(UIView *)view
//{
//    UIGraphicsBeginImageContextWithOptions(view.bounds.size, YES, 0);
//    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    return image;
//}


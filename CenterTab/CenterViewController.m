//
//  CenterViewController.m
//  CenterTab
//
//  Created by JOSEPH KERR on 5/22/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import "CenterViewController.h"

//@interface CenterViewController ()
//@property (strong, nonatomic) UIView *magicView;
//@end

@implementation CenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)doneTapped:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    [_delegate didDismiss];
    
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


-(void)setMagicView:(UIView *)magicView {
    if (_magicView != nil) {
        [_magicView removeFromSuperview];
    }
    
    _magicView = magicView;
    
    [self.view addSubview:self.magicView];
}

//-(void)addTrans:(UIView *)magicView {
//    self.transitionView.hidden = YES;
//    self.magicView = magicView;
//    [self.view addSubview:self.magicView];
//}

-(void)magic {
    //CGRect newFr = tabBarController.bounds;
    CGRect newFr = CGRectOffset(self.view.bounds, 0, self.view.bounds.size.height);
    
//        [UIView animateWithDuration:2.0
//                              delay:0
//             usingSpringWithDamping:300.0
//              initialSpringVelocity:5.0
//                            options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState
//                         animations:^{
//                             
//                             self.magicView.frame = newFr;
//                         }
//                         completion:^(BOOL finished){
//                             
//                             [self.magicView removeFromSuperview];
//                         }];

    
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.magicView.frame = newFr;
    } completion:^(BOOL finished){
        [self.magicView removeFromSuperview];
    }];
}

@end
